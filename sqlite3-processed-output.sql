PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE "companies" (
	`name`	TEXT NOT NULL,
	`location`	TEXT,
	`telephone`	TEXT,
	`fax`	TEXT,
	`address`	TEXT,
	`category`	TEXT,
	PRIMARY KEY(name)
);
CREATE TABLE "files" (
	`file`	TEXT NOT NULL,
	`md5sum`	TEXT NOT NULL,
	`processed`	INTEGER NOT NULL DEFAULT CURRENT_TIMESTAMP
);
COMMIT;
