# Mirror and Process yellowpages.bh
Set of scripts to mirror yellowpages.bh website and process the retrieved pages extracting companies information and saving in to a Sqlite3 database.

## Requirements: 
These packages are required to run the scripts. Additional libraries might be required for the python script. See sources.
- httrack
- python3

## Files and description
- get-httrack.sh: Use httrack to mirror the yellowpages.bh website
- chrome-console.test.js: Used this to test initial logic for extraction of information
- sqlite3-processed-output.sql: SQL file to genreate the sqlite3 database for saving extracted information
- process.py: Read HTML file from mirror and put extracted information in to database
- yp_config.py: Static configuration key value pairs for use by the process.py script

