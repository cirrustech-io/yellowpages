/*
 * 
 * Written to figure out how to extract relevant information from yellowpages.bh pages
 * This can be run in Chrome debug console
 * 
 * Ali Khalil
 * 2015-10-27
 * 
 */

divs = document.getElementsByClassName('single-item')
props = [ 'name', 'location', 'telephone', 'faxNumber', 'streetAddress' ]
data = Array();
for ( i=0; i<divs.length; i++ ){
	item = divs[i].getElementsByTagName('*');
	temp = Array()
	for ( j=0; j<item.length; j++ ) {
		thisprop = item[j].getAttribute('itemprop');
		if ( thisprop != null && props.indexOf( thisprop ) != -1 ) {
			temp[thisprop] = item[j].innerText;
		}
	}
	data[temp['name']] = temp;
}
console.log( data );
