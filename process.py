#!/usr/bin/env python3

import os
from lxml import html
import sqlite3
import hashlib
from yp_config import props, root_dir, sqlite_db

# initialize variables
data = []

# sqlite3 dbconnection
def get_db_connect():
    conn = sqlite3.connect( sqlite_db )
    curs = conn.cursor()
    return conn, curs

# Store data function
def store_data(data, md5sum, file_path):
    # Put data in to sqlite3 database
    conn, curs = get_db_connect()
    curs.execute( '''INSERT INTO files ( file, md5sum ) VALUES( '%s', '%s' );'''%( file_path, md5sum ) )
    curs.executemany('''INSERT OR IGNORE INTO companies (name,telephone,fax,address,location,category) VALUES (?,?,?,?,?,?);''', data )
    conn.commit()
    conn.close()

# walking through dir and subdir and file
def walk_through_dir(root_dir):
    for subdir, dirs, files in os.walk(root_dir):
        for file in files:
            file_path = os.path.join(subdir, file)
            print (file_path)
            extract_data(file_path)

# extract data from files
def extract_data(file_path):
    # encoding must be specified at latin-1 or the read operation will fail in python3
    contents = open(file_path,'r', encoding="latin-1").read()

    # generate the md5sum of file
    m = hashlib.md5()
    m.update(contents.encode('utf-8'))
    md5sum = m.hexdigest()

    try:
        # set up the xml tree
        tree = html.fromstring(contents)
    except Exception as e:
        print( '''FAIL: %s'''%(file_path) )
        return False

    # load all div elements with class=single-item
    companies = tree.xpath('//div[@class="single-item"]')

    # start processing each div and extract contianed information
    for company in companies:
		
        # temporary variable to store 
        company_data = {}
		
        # Process elements in the div
        for child in company.iterdescendants(tag=None):
		
            # Only interested in elements with attribute "itemprop"
            property = child.get('itemprop', default=None )
			
	    # Only interested in properties in list
            if property and property in props:
                company_data[property] = child.text
			
                # Category element doesn't have itemprop
                # But, the category is specified in the sibling <p> of <p> which contains property name as child
                if property == 'name':
                    for ancestor in child.iterancestors():
                        if ancestor.tag == 'p':
                            company_data['category'] = ancestor.getnext().text
                            break
				
			
        # Save data to list
        data.append( 
            ( 
            company_data['name'] if 'name' in company_data.keys() else None, 
            company_data['telephone'] if 'telephone' in company_data.keys() else None,
            company_data['faxNumber'] if 'faxNumber' in company_data.keys() else None,
            company_data['streetAddress'] if 'streetAddress' in company_data.keys() else None,
            company_data['location'] if 'location' in company_data.keys() else None,
            company_data['category'] if 'category' in company_data.keys() else None,
            )
        )

    if data:
        store_data(data, md5sum, file_path)



if __name__ == '__main__':
    walk_through_dir(root_dir)	


