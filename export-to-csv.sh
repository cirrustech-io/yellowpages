#!/bin/bash

DATABASE=$1

if [[ -z $DATABASE  ]]; then
	echo "No database file specified."
	echo
	echo "Usage: $0 DATABASE_FILE"
	echo
	exit 1
fi

if [[ ! -f  $DATABASE ]]; then
	echo
	echo "File not found: $DATABASE"
	echo
	exit 1
fi

sqlite3 -header -csv $DATABASE "SELECT * FROM companies;" > companies_$(date +%Y%m%d_%H%M%S).csv

exit 0

