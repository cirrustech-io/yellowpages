#!/bin/bash -x

SOURCE_URL="http://yellowpages.bh"
OUTPUT_DIRECTORY="~/websites/yellowpages.bh"

httrack -q -%i -iC2 $SOURCE_URL -O $OUTPUT_DIRECTORY -n -%P -p7 -N0 -s0 -o -x -p1 -D -a -K0 -c8 -%k -A25000 -*.png -*.gif -*.jpg -*.jpeg -*.css -*.js -ad.doubleclick.net/* -*.ico -*/webBH/company/* -*/uploads/* -*/images/* -*/webBH/0*/* -*/webBH/1*/* -*/webBH/2*/* -*/webBH/3*/* -*/webBH/4*/* -*/webBH/5*/* -*/webBH/6*/* -*/webBH/7*/* -*/webBH/8*/* -*/webBH/9*/* -*/webBH/register/* -*/webBH/contact-form/* -*/webBH/register/* -%s -%u -k -f2 -z
